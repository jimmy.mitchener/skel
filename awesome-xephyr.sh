#!/bin/bash
# Run Awesome in a nested server for tests
#
# Requirements: (Debian)
#
#  apt-get install xserver-xephyr
#  apt-get install -t unstable awesome
#
# Based on original script by dante4d <dante4d@gmail.com>
# See: http://awesome.naquadah.org/wiki/index.php?title=Using_Xephyr
#
# URL: http://hellekin.cepheide.org/awesome/awesome_test
#
# Copyright (c) 2009 Hellekin O. Wolf <hellekin@cepheide.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 

# If rc.lua.new is missing, make a default one.
RC_LUA=skel/dot.config/awesome/rc.lua
test -f $RC_LUA

# Just in case we're not running from /usr/bin
AWESOME=`which awesome`
XEPHYR=`which Xephyr`

test -x $AWESOME || { echo "Awesome executable not found. Please install Awesome"; exit 1; }
test -x $XEPHYR || { echo "Xephyr executable not found. Please install Xephyr"; exit 1; }

$XEPHYR -ac -br -noreset -screen 800x600 :1 &
sleep 1
DISPLAY=:1.0 $AWESOME -c $RC_LUA &
