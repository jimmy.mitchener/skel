# Skeleton Files

These files are to be used as the base configuration for a GNU/Linux development environment.

## Vim Plugins

Pathogen is being used to bundle Vim plugins. New plugins are best installed using a git submodule as follows:

    git submodule add [git url] skel/dot.vim/bundle/[plugin name]

All submodules are updated upon running the install script.
