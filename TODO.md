# Skel TODO

A basic list of things I'd like to do with my current config

## Rxvt-Unicode

- Live profile switching (-name in Xresources)
- Figure out how to open a new window in the same directory (ctrl-shift-n in gnome-terminal)

## Vim

- Fugitive `:Gcommit` being wierd in terminal especially with split windows

## Awesome

- Remove all gnome dependencies
- Make current screen more obvious
- Moar widgets!
    - Networking
    - Volume
