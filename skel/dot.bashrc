#!/bin/bash
# ----------------------------------------------------------
#
# Copyright (c) 2011 Jim Mitchener <jcm@packetpan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# ----------------------------------------------------------

# Only setup terminals and things when interactive
if [ -n "$PS1" ]; then

    # don't put duplicate lines in the history. See bash(1) for more options
    export HISTCONTROL=ignoredups

    # check the window size after each command and, if necessary,
    # update the values of LINES and COLUMNS.
    shopt -s checkwinsize

    # Append history to ~/.bash_history instead of replacing it
    shopt -s histappend

    # Include .* in globs and extended glob syntax
    shopt -s dotglob extglob 

    # make less more friendly for non-text input files, see lesspipe(1)
    [ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

    export VISUAL='vim'
    export EDITOR='vim'

    # Some handy aliases
    alias c=clear
    alias l=less
    alias cd..='cd ..'
    alias df='df -h'
    alias du='du -h'
    alias tree='tree -Fsh'
    alias igrep='grep -i'
    alias rgrep='grep -r'

    alias dos2unix="perl -pi -e 's/\r\n/\n/'"
    # gnome-screensaver doesn't support proper xscreensaver api
    alias mplayer='mplayer -stop-xscreensaver -heartbeat-cmd "gnome-screensaver-command -p"'
    alias mplayer-fast='mplayer -cache 16384 -vfm ffmpeg -lavdopts lowres=1:fast:skiploopfilter=all'
    alias rsync='rsync -avhP'

    # -------------------
    # The colors, Duke! The colors!!!!
    # -------------------

    BLACK='\[\e[0;30m\]'
    RED='\[\e[0;31m\]'
    GREEN='\[\e[0;32m\]'
    YELLOW='\[\e[0;33m\]'
    BLUE='\[\e[0;34m\]'
    CYAN='\[\e[0;36m\]'
    PURPLE='\[\e[;35m\]'
    WHITE='\[\e[0;37m\]'

    BBLACK='\[\e[1;30m\]'
    BRED='\[\e[1;31m\]'
    BGREEN='\[\e[1;32m\]'
    BYELLOW='\[\e[1;33m\]'
    BBLUE='\[\e[1;34m\]'
    BPURPLE='\[\e[1;35m\]'
    BCYAN='\[\e[1;36m\]'
    BWHITE='\[\e[1;37m\]'

    NORM='\[\e[0;00m\]' 

    function parse_git_branch() {
        ref=$(git symbolic-ref HEAD 2> /dev/null) || return
        ref=${ref#refs/heads/}

        case "$ref" in
        master)
            color="\e[00;31m" #red
            ;;
        *)
            color="\e[00;32m" #green
            ;;
        esac

        echo -e "${color} (${ref})"
    }

    if [ $UID = "0" ]; then #root user
        export PS1="$RED\u@\h:\w # $NORM"
    else
        export PS1="$CYAN\u$GREEN@$CYAN\h$GREEN:$BLUE\w\$(parse_git_branch) $GREEN> $NORM"
        export PS2="$GREEN  > $NORM"
    fi

    # If this is an xterm set the title to user@host:dir
    case "$TERM" in
    xterm*|rxvt*)
        PROMPT_COMMAND='echo -ne "\e]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
        ;;
    *)
        ;;
    esac

    # enable color support of ls and also add handy aliases
    if [ `uname` = "Darwin" ]; then
        alias ls='ls -FG'
    else
        if [ "$TERM" != "dumb" ]; then
            eval "`dircolors -b`"
            alias ls='ls -FXsh --color=auto'
        else
            alias ls='ls -FXsh'
        fi
    fi

    # -------------------
    # Useful utility functions I've developed [come to rely upon] over time
    # -------------------

    # Handy means of grepping ps
    psgrep() {
        if [ $# != "1" ]; then
            echo "Usage: psgrep PATTERN" >&2
            return 1
        fi

        ps aux |grep -i $1 |grep -v grep
    }

    # Better than pkill
    pskill() {
        if [ $# = "0" ]; then
            echo "Usage: pskill PID..." >&2
            return 1
        fi

        pids=`psgrep $1 | awk '{print $2}'`
        for pid in $pids; do
            echo -n "killing $pid ... ";
            results=`kill -9 $pid 2>&1`;
            if [ $? == "0" ]; then
                echo "done.";
            else
                echo "failed: $results";
            fi
        done
    }

    # Grep svn like git grep
    # TODO - allow passing of options to grep
    sgrep() {
        if [ $# = "0" ]; then
            echo "Usage: sgrep PATTERN [FILE...]" >&2
            return 1
        fi

        # FIXME - Should only have one grep call with $args or something.
        #         Couldn't get it to work (kept expanding $@)
        if [ $# = "1" ]; then
            grep -Ri --exclude-dir='.svn' "$@" *
        else
            grep -Ri --exclude-dir='.svn' "$@"
        fi
    }

    # -----------------------
    # Audio Conversion Scripts
    # -----------------------

    # TODO - These need to be modified to maintain ID3 tag information

    # Convert Monkey Audio (.ape) to FLAC
    mac2flac() {
        if [ $# != "1" ]; then
            echo "Usage: mac2flac FILE" 1>&2
            return 1
        fi

        mac "$1" - -d |flac -8 -o "${1%.*}.flac" -
    }

    # Convert FLAC to 320kbps VBR MP3
    flac2mp3() {
        if [ $# != "1" ]; then
            echo "Usage: flac2mp3 FILE" 1>&2
            return 1
        fi

        flac -dc "$1" |lame -h -V 3 -b 128 -B 320 - "${1%.*}.mp3"
    }

fi # interactive

# Setup PATH
PATH="$PATH:/usr/local/sbin:/usr/sbin:/sbin"

# Include user's bin if it exists
[ -d ${HOME}/bin ] && PATH="${HOME}/bin:$PATH"

export PATH

# So mplayer uses ICC profiles without compiz
export VDPAU_NVIDIA_NO_OVERLAY=1

export TZ="America/Anchorage";

# Load RVM
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
# Enable Tab Completion in RVM
[[ -r $HOME/.rvm/scripts/completion ]] && source $HOME/.rvm/scripts/completion

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Source homebrew's completion if present
if [ -x /usr/local/bin/brew ]; then
    if [ -f `brew --prefix`/etc/bash_completion ]; then
        . `brew --prefix`/etc/bash_completion
    fi
fi

# Allow for local modifications
[ -f ~/.bashrc.local ] && . ~/.bashrc.local

# vim:ft=sh
