# vim:ft=zsh

setopt histignorealldups sharehistory

# cd act like pushd
setopt autopushd

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 10000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

autoload -U colors
colors

# Make path elements unique. path array backs PATH
typeset -U path

# Fix word boundaries to be more like readline
autoload -U select-word-style
select-word-style bash

# Window / Screen title where appropriate
case $TERM in
    *xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
        # Use rxvt-unicode for TERM so remote servers understand
        [[ $TERM = "rxvt-unicode-256color" ]] && TERM=rxvt-unicode
        __jcm_term_title () { print -Pn "\e]0;%n@%M [%~]\a" }
        ;;
    screen*)
        __jcm_term_title () {
            print -Pn "\e]83;title \"$1\"\a"
            print -Pn "\e]0;$TERM - (%L) %n@%M [%~] ($1)\a"
        }
        ;;
    *) # NOP to remove console warning
        __jcm_term_title () {}
    ;;
esac

function parse_git_branch() {
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return
    ref=${ref#refs/heads/}

    case "$ref" in
    master)
        color="$fg[red]" #red
        ;;
    *)
        color="$fg[green]" #green
        ;;
    esac

    echo -e "%{${reset_color}%}%{${color}%} ${ref}"
}

# Called right before the prompt is rendered
function precmd() {
    __jcm_term_title
    PROMPT="%{$fg[cyan]%}%n@%m%{$reset_color%} %{$fg[blue]%}%~$(parse_git_branch)%{$reset_color%} %# "
}

function preexec() {
    __jcm_term_title
}

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

export VISUAL='vim'
export EDITOR='vim'
export MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"

# Some handy aliases
alias l=less
alias r=rails
alias g=git
alias d='dirs -v'
alias cd..='cd ..'
alias df='df -h'
alias du='du -h'
alias tree='tree -Fsh'
alias igrep='grep -i'
alias rgrep='grep -r'
alias startx='exec startx' # So killing X doesn't result in a shell

# Don't convert hostnames/ports
alias lsof='lsof -nP'

# 256 color tmuX
alias tmux='tmux -2'

alias dos2unix="perl -pi -e 's/\r\n/\n/'"
# gnome-screensaver doesn't support proper xscreensaver api
alias mplayer='mplayer -lavdopts threads=4 -stop-xscreensaver -heartbeat-cmd "gnome-screensaver-command -p"'
alias mplayer-fast='mplayer -cache 16384 -vfm ffmpeg -lavdopts lowres=1:fast:skiploopfilter=all'
alias rsync='rsync -avhP'

# Stop popping in pulse 4
alias skype='env PULSE_LATENCY_MSEC=60 skype'

# Clean up exited containers (why is this not builtin yet?!)
alias docker-cleanup='docker rm $(docker ps -aq -f "status=exited")'

# enable color support of ls and also add handy aliases
if [ `uname` = "Darwin" ]; then
    alias ls='ls -FG'
else
    if [ "$TERM" != "dumb" ]; then
        if [ -f ~/.dircolors ]; then
            eval "`dircolors -b ~/.dircolors`"
        else
            eval "`dircolors -b`"
        fi

        alias ls='ls -FXsh --color=auto'
    else
        alias ls='ls -FXsh'
    fi
fi

# -------------------
# Useful utility functions I've developed [come to rely upon] over time
# -------------------

# Handy means of grepping ps
psgrep() {
    if [ $# != "1" ]; then
        echo "Usage: psgrep PATTERN" >&2
        return 1
    fi

    # grep -v to prevent our grep from showing up in results
    ps aux |grep -iE "(^USER)|($1)" |grep -v "grep -iE (^USER)"
}

# Better than pkill
pskill() {
    if [ $# = "0" ]; then
        echo "Usage: pskill PID..." >&2
        return 1
    fi

    pids=$(ps aux | grep -i "$1" | grep -v "grep -i $1" | awk '{print $2}')
    for pid in $pids; do
        echo -n "killing $pid ... ";
        results=`kill -9 $pid 2>&1`;
        if [[ $? == "0" ]]; then
            echo "done.";
        else
            echo "failed: $results";
        fi
    done
}

# Grep svn like git grep
# TODO - allow passing of options to grep
sgrep() {
    if [ $# = "0" ]; then
        echo "Usage: sgrep PATTERN [FILE...]" >&2
        return 1
    fi

    # FIXME - Should only have one grep call with $args or something.
    #         Couldn't get it to work (kept expanding $@)
    if [ $# = "1" ]; then
        grep -Ri --exclude-dir='.svn' "$@" *
    else
        grep -Ri --exclude-dir='.svn' "$@"
    fi
}

# -----------------------
# Audio Conversion Scripts
# -----------------------

# TODO - These need to be modified to maintain ID3 tag information

# Convert Monkey Audio (.ape) to FLAC
mac2flac() {
    if [ $# != "1" ]; then
        echo "Usage: mac2flac FILE" 1>&2
        return 1
    fi

    mac "$1" - -d |flac -8 -o "${1%.*}.flac" -
}

# Convert FLAC to 320kbps VBR MP3
flac2mp3() {
    if [ $# != "1" ]; then
        echo "Usage: flac2mp3 FILE" 1>&2
        return 1
    fi

    flac -dc "$1" |lame -h -V 3 -b 128 -B 320 - "${1%.*}.mp3"
}

git-svn-diff() {
    git diff --no-prefix --ignore-space-at-eol $@ | sed -e "s/^diff --git [^[:space:]]*/Index:/" -e "s/^index.*/===================================================================/"
}

# Append sbin dirs to path array
path+='/sbin'
path+='/usr/sbin'
path+='/usr/local/sbin'

# Prepend user bin if it exists
[ -d ${HOME}/bin ] && path[1,0]="${HOME}/bin"

# path array backs PATH still need to export
export PATH

# So mplayer uses ICC profiles without compiz
export VDPAU_NVIDIA_NO_OVERLAY=1

export TZ="US/Pacific-New";

# Load RVM
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

# Allow for local modifications
[ -f ~/.zshrc.local ] && . ~/.zshrc.local
