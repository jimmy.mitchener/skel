set guicursor=n-v-c:block-Cursor/lCursor,ve:ver35-Cursor,o:hor50-Cursor,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor,sm:block-Cursor-blinkwait175-blinkoff150-blinkon175,a:blinkon0
set guioptions=agit " Remove nearly everything from the gvim
set guifont=DejaVu\ Sans\ Mono\ 10

colo molokai

" Allow for local machine-specific modifications
if filereadable($HOME . "/.gvimrc.local")
    source ~/.gvimrc.local
endif
