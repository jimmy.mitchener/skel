" vim: set ft=vim :

" Pathogen Init
execute pathogen#infect()

" Map :W to :w for convenience
command! W :w

" Sick of typing this by mistake
command! Noh :noh

" Change <Leader> to something more useful
let mapleader = ","

" Syntax highlighting
syn on

" Don't highlight C++ keywords as errors in Java (aka delete)
let java_allow_cpp_keywords = 1

" Auto indent
set ai
set ci

" Ignore case in searches
"set ic

" Set tab/shift-width and expanding (soft) tabs
set ts=4
set sw=4
set expandtab

" Maintain selection when fixing indentation
vnoremap > >gv
vnoremap < <gv
vnoremap = =gv

" Horizontal scrolling
noremap <C-L> zl
noremap <C-H> zh

" Nerd Tree
nnoremap <Leader>nt :NERDTree<CR>

" Fuzzy Finder
nnoremap <Leader>fc :FufCoverageFile<CR>
nnoremap <Leader>fr :FufRenewCache<CR>
nnoremap <Leader>fb :FufBuffer<CR>
nnoremap <Leader>fl :FufLine<CR>
nnoremap <Leader>ft :FufTaggedFile<CR>

" Tagbar
nnoremap <Leader>tb :TagbarToggle<CR>

" Extensions for Fuf to skip in coverage-file mode
let g:fuf_coveragefile_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp|class|jar)$|(^|[/\\])((\.(hg|git|bzr|svn))|(target|dist|build|vendor))($|[/\\])'

" Fugitive (git front-end)
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>gc :Gcommit<CR>
nnoremap <Leader>gb :Gblame<CR>
nnoremap <Leader>gg :Ggrep<CR>
nnoremap <Leader>gd :Gdiff<CR>

" Allow . support for surround and other plugins
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)

" Enabled filetype
filetype plugin indent on

" Use javascript syntax for ActionScript
au BufNewFile,BufRead *.as set ft=javascript

" Use scala syntax for SBT (Simple Build Tool) files
au BufNewFile,BufRead *.sbt set ft=scala

" Open vagrant files as ruby
au BufNewFile,BufRead Vagrantfile set ft=ruby

" Use 2 space tabs for certain languages
autocmd FileType ruby,eruby,javascript,html set sw=2 sts=2 et

" Ruby/Rails Completion
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1

" Use syntaxcomplete for everything not specified
if has("autocmd") && exists("+omnifunc")
    autocmd Filetype *
        \   if &omnifunc == "" |
        \       setlocal omnifunc=syntaxcomplete#Complete |
        \   endif
endif

" Highlight whitespace chars
set list listchars=tab:>-,trail:.

set cpoptions=aABceFsM
set backspace=indent,eol,start
set fileencodings=ucs-bom,utf-8,latin1
set formatoptions=tcql
set helplang=en
set history=50

set modeline        " enable vim:set ft=foo: style comments
set modelines=5     " Number of lines to check for modeline

set cursorline      " Highlight current line

set linespace=1     " space lines for easier reading

set incsearch       " find next match as we search
set hlsearch        " highlight searches

set mousehide       " Hide mouse when typing
set mouse=a         " Enable terminal mouse

set laststatus=2    " always show status line

set number          " Line numbers

set t_Co=256        " Set 256 color terminal
set bg=dark         " Must set for solarized
colo solarized      " Awesome color scheme

set termencoding=utf-8
set viminfo='20,\"50
set window=60

set statusline=%<%f\ %m%r%y

" Git branch
set statusline+=%{fugitive#statusline()}

" right align
set statusline+=%=

" File encoding,format
set statusline+=[%{strlen(&fenc)?&fenc:'none'},%{&ff}]\ 

set statusline+=%l,%c
set statusline+=%8(%P%)

" Allow for local machine-specific modifications
if filereadable($HOME . "/.vimrc.local")
    source ~/.vimrc.local
endif
