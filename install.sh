#!/bin/bash

INSTALLDIR=$HOME

echo -n "Are you sure you want to install skel files in $INSTALLDIR? [y/N] "

read yesno

if [[ $yesno != "y" && $yesno != "Y" ]]; then
    echo "Alright, you're safe for now!" >&2
    exit 0
fi

check_status() {
    if [ $? = "0" ]; then
        echo "done."
    else
        echo "failed: $err"
    fi
}

echo -n "Updating git submodules... "

err=$(git submodule init 2>&1 && git submodule update 2>&1)
check_status $err

cd skel

for x in *; do
    echo -n "Installing ${x#dot} ... "

    if [ -d $x ]; then
        mkdir -p $INSTALLDIR/${x#dot}
        err=$( cp -rf $x/* $INSTALLDIR/${x#dot}/ 2>&1 )
    else
        err=$(cp -rf $x $INSTALLDIR/${x#dot} 2>&1)
    fi

    check_status $err
done

if [ $(uname) != "Darwin" ]; then
    echo -n "Reloading Xresources ... "

    err=$(xrdb -load ~/.Xresources 2>&1)
    check_status $err
fi

touch_files() {
    touch ~/.tmux.conf.local
}

echo -n "Touching files that break if they're not present... "
err=$(touch_files)
check_status $err

echo -n "Sourcing new tmux config... "
err=$(tmux source ~/.tmux.conf)
check_status $err
